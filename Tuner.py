# -*- coding: utf-8 -*-
"""
git clone https://github.com/keras-team/keras-tuner.git
"""
from tensorflow.keras.datasets import fashion_mnist
import matplotlib.pyplot as plt
from tensorflow import keras
from tensorflow.keras.layers import  Conv2D, MaxPooling2D, Dense, Flatten, Activation

from kerastuner.tuners import RandomSearch
from kerastuner.engine.hyperparameters import HyperParameter
import time
import os

LOG_DIR = f"{int(time.time())}"



(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

x_train = x_train.reshape(-1,28,28,1)
x_test = x_test.reshape(-1,28,28,1)

def build_model(hp):   #random search passes this hyperparameter() object
    model = keras.models.Sequential()
    
    
    #model.add(Conv2D(32, (3, 3), input_shape=x_train.shape[1:]))
    model.add(Conv2D(hp.Int("input_units", min_value=32, max_value=256, step=32), (3,3), input_shape = x_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    
    for i in range(hp.Int("n_layers",min_value = 1, max_value = 4, step=1)):
        #model.add(Conv2D(32, (3, 3)))                
        model.add(Conv2D(hp.Int(f"conv_{i}_units", min_value=32, max_value=256, step=32), (3,3)))
        model.add(Activation('relu'))
        #model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    
    model.add(Dense(10))
    model.add(Activation("softmax"))
    
    model.compile(optimizer="adam",
                  loss="sparse_categorical_crossentropy",
                  metrics=["accuracy"])
    return model

tuner = RandomSearch(build_model,
                     objective = "val_accuracy",
                     max_trials = 5, #NUMBER OF TUNNING TRIALS
                     executions_per_trial=1, #BEST PERFOMANCE SET TO 3+
                     directory= os.path.normpath('C:/'),# there is a limit of characters keep path short
                     overwrite=True #need this to override model when testing
                     )

tuner.search(x=x_train,
             y=y_train, 
             epochs=1,
             batch_size=64,
             validation_data=(x_test,y_test),)
